Instalation steps
-----

sudo apt install ncftp
sudo apt install python3-sphinx
sudo pip3 install recommonmark
sudo pip3 install sphinx-rtd-theme
sudo pip3 install PyStemmer

Make it
---------

make html

To convert pages from MD to RST
--------

Of source: https://bfroehle.com/2013/04/26/converting-md-to-rst/


Converting marktown to rst
---------

```
sudo apt install pandoc
pandoc --from=markdown --to=rst --output=telescoala.rst source/ytvideos/telescoala.md 
```
