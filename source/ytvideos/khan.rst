Khan Academy Mathematics Videos
===============================

.. figure:: ../images/khan-academy-open-learning.png
   :alt: Khan Academy Logo

Clasa VI
--------

-  `Numere întregi - Semestrul 2
   Algebră <https://www.khanacademy.org/math/cc-sixth-grade-math/cc-6th-negative-number-topic>`__
-  `Geometrie - Triunghiul și proprietățile
   triunghiului <https://www.khanacademy.org/math/cc-eighth-grade-math/cc-8th-geometry/cc-8th-triangle-angles/>`__

Clasa VII
---------

-  `Numere iraționale - Semestrul 1
   Algebră <https://www.khanacademy.org/math/algebra/x2f8bb11595b61c86:irrational-numbers>`__
-  `Sisteme de ecuații - Semestrul 2
   Algebră <https://www.khanacademy.org/math/algebra/x2f8bb11595b61c86:systems-of-equations>`__
-  `Ecuații/Inegalități - Semestrul 2
   Algebră <https://www.khanacademy.org/math/algebra/x2f8bb11595b61c86:solve-equations-inequalities>`__
-  `Teorema lui Pitagora - Semestrul 2
   Geometrie <https://www.khanacademy.org/math/cc-eighth-grade-math/cc-8th-geometry/cc-8th-pythagorean-theorem/v/the-pythagorean-theorem>`__
-  `Aplicații Teorema lui Pitagora - Aria unui triunghi isoscel -
   Semestrul 2
   Geometrie <https://www.khanacademy.org/math/cc-eighth-grade-math/cc-8th-geometry/pythagorean-theorem-application/v/area-of-an-isosceles-triangle>`__
-  `Aplicații Teorema lui Pitagora - Distanțe - Semestrul 2
   Geometrie <https://www.khanacademy.org/math/cc-eighth-grade-math/cc-8th-geometry/pythagorean-distance/v/distance-formula>`__

Clasa VIII
----------

-  `Radicali cu calcule cu puteri cu exponent negativ - Semestrul 1,
   Algebra <https://www.khanacademy.org/math/algebra/x2f8bb11595b61c86:rational-exponents-radicals>`__
-  `Funcții - Semestrul 2
   Algebră <https://www.khanacademy.org/math/algebra/x2f8bb11595b61c86:functions>`__
-  `Corpuri rotunde - Semestrul 2
   Geometrie <https://www.khanacademy.org/math/cc-eighth-grade-math/cc-8th-geometry/cc-8th-volume/v/cylinder-volume-and-surface-area>`__
