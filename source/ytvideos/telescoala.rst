Teleșcoala TVR
==============

.. figure:: ../images/generic-telescoala.jpg
   :alt: TeleScoala
   :width: 300
   :height: 300

Playlist oficial TVR
--------------------

.. figure:: ../images/TVR.png
   :alt: TVR
   :width: 50
   :height: 50

-  `Playlist Youtube - Teleșcoala 2020-2021 <https://www.youtube.com/watch?v=AgJ6CKpeTMQ&list=PLxO8-C91Lp93X84425vP2xzM6dna8jFVj>`__
-  `Playlist Youtube - Teleșcoala la TVR2 - 2020 <https://www.youtube.com/watch?v=6GKzsjFnAEM&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU>`__
-  `Playlist Youtube - Teleșcoala la TVR2 - 2021 <https://www.youtube.com/playlist?list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU>`__
-  `Articole teleșcoala TVR <http://www.tvr.ro/telescoala.html>`__
-  `Pagina de facebook - telescoalaTVR <https://www.facebook.com/telescoalaTVR/>`__

.. role:: red

.. Mai jos aveți organizate pe clase :red:`lecțiile doar de matematică` din cadrul Teleșcoala TVR publicate până acum

.. Mate Clasa VIII
.. ----------------


.. Teste de evaluare
.. ~~~~~~~~~~~~~~~~~


.. Mate Clasa XII
.. ---------------

.. .. _lecții-1:

.. Lecții
.. ~~~~~~

.. -  `Aplicații ale integralei definite: Calcul de arii și
..    volume <https://www.youtube.com/watch?v=xxXsnt7WV5Q&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=73&t=0s>`__
.. -  `Inegalități
..    integrale <https://www.youtube.com/watch?v=hMkK4vCfJgU&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=98&t=0s>`__
.. -  `Polinoame. Relațiile lui Viete <https://youtu.be/YXnJRnGfhJ8>`__

.. .. _teste-de-evaluare-1:

.. Teste de evaluare
.. ~~~~~~~~~~~~~~~~~

.. -  `Model de test bacalaureat I - partea
..    I <https://www.youtube.com/watch?v=Zes1FelMnDY&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=110&t=0s>`__
.. -  `Model de test bacalaureat - subiectul al
..    II-lea <https://www.youtube.com/watch?v=jft0cxyThu0&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=118&t=0s>`__
.. -  `Model test bacalaureat - subiectul al
..    II-lea <https://www.youtube.com/watch?v=0amojMKuvEw&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=123&t=0s>`__
.. -  `Model test bacalaureat - subiectul al
..    III-lea <https://www.youtube.com/watch?v=m8kuOPT8Ajk&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=127&t=0s>`__
.. -  `Model 2020 - subiect
..    3 <https://www.youtube.com/watch?v=3XtD6dDh2-s&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=142&t=0s>`__
.. -  `Model 2 test bac - subiect
..    3 <https://www.youtube.com/watch?v=FKixn7HVx5o&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=146&t=0s>`__
