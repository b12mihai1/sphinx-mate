
# Lecții video "Rezolvare teste"

## Cls VIII: Rezolvarea testelor de antrenament

Următoarele videoclipuri conțin rezolvările și explicațiile pentru testele de antrenament publicate pe ROCNEE:
[https://rocnee.eu/testeantrenament/](https://rocnee.eu/testeantrenament/)

Subiectele de matematica (cele 40 de teste de antrenament) se pot accesa aici: [http://s.go.ro/inwgz0z1](http://s.go.ro/inwgz0z1)

<ul>
<li>
<a href="https://youtu.be/QgtL5coM6SA">Rezolvarea testului 1</a>
<br />
<iframe width="560" height="315" src="https://www.youtube.com/embed/QgtL5coM6SA"
 frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</li>
<br />
<br />

<li>
<a href="https://youtu.be/tTmDZxYcm9I">Rezolvarea testului 2</a>
<br />
<iframe width="560" height="315" src="https://www.youtube.com/embed/tTmDZxYcm9I"
 frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</li>
<br />
<br />

<li>
<a href="https://youtu.be/1vmQVPDEuGw">Rezolvarea testului 3</a>
<br />
<iframe width="560" height="315" src="https://www.youtube.com/embed/1vmQVPDEuGw"
 frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</li>
<br />
<br />

<li>
<a href="https://youtu.be/CiJ_USFWTwY">Rezolvarea testului 4</a>
<br />
<iframe width="560" height="315" src="https://www.youtube.com/embed/CiJ_USFWTwY"
 frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</li>
<br />
<br />

<li>
<a href="https://youtu.be/i-veh5JMls8">Rezolvarea testului 5</a>
<br />
<iframe width="560" height="315" src="https://www.youtube.com/embed/i-veh5JMls8"
 frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</li>
<br />
<br />

<li>
<a href="https://www.youtube.com/watch?v=O_V_j3_3BUw">Rezolvarea testului 6</a>
<br />
<iframe width="560" height="315" src="https://www.youtube.com/embed/O_V_j3_3BUw"
 frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</li>
<br />
<br />

<li>
<a href="https://www.youtube.com/watch?v=Y7SOCtf0TuA">Rezolvarea testului 7</a>
<br />
<iframe width="560" height="315" src="https://www.youtube.com/embed/Y7SOCtf0TuA"
 frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</li>
<br />
<br />

<li>
<a href="https://www.youtube.com/watch?v=6soA3DlL7nw">Rezolvarea testului 8</a>
<br />
<iframe width="560" height="315" src="https://www.youtube.com/embed/6soA3DlL7nw"
 frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</li>
<br />
<br />

<li>
<a href="https://www.youtube.com/watch?v=ivSfmzTmMvQ">Rezolvarea testului 9</a>
<br />
<iframe width="560" height="315" src="https://www.youtube.com/embed/ivSfmzTmMvQ"
 frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</li>
<br />
<br />

<li>
<a href="https://www.youtube.com/watch?v=gRT2X0WgFh0">Rezolvarea testului 10</a>
<br />
<iframe width="560" height="315" src="https://www.youtube.com/embed/gRT2X0WgFh0"
 frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</li>

</ul>