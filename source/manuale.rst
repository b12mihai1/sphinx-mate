Manuale
=======

.. figure:: images/klementinum.jpg
   :alt: Klementinum Library Prague
   :width: 500
   :height: 500

*Biblioteca* `Klementinum <https://www.klementinum.com/en/>`__ *din Praga. Sursa imagine: Pinterest*

Manuale aprobate MEC
--------------------

.. figure:: images/logo-MEC-2020.png
   :alt: MEC
   :width: 150
   :height: 150

-  Manualele aprobate MEC se găsesc pe `manuale.edu.ro <http://manuale.edu.ro>`__
   -  Aici: `digistorage-manuale <http://s.go.ro/hv1xumf0>`__ există o clonă locală a site-ului ministerului cu manualele aprobate

Manuale vechi
-------------

.. figure:: images/oldbooks.png
   :alt: MEC
   :width: 200
   :height: 200

-  `E. Rogai - Tabele și formule matematice,
   1983 <http://s.go.ro/8wpr93a0>`__
-  Clonă locală cu toate manualele vechi de matematică `aici -
   link <http://s.go.ro/a0nex500>`__

Manuale vechi gimanziu
----------------------

-  `Matematică - Clasa V EDP 1989 <http://manualul.info/Mate_V/>`__
-  `Algebră - Clasa VI EDP 1989 <http://manualul.info/Alg_VI/>`__
-  `Geometrie - Clasa VI EDP
   1989 <http://manualul.info/Geom_VI_1989/>`__
-  `Algebră - Clasa VII EDP 1995 <http://manualul.info/Alg_VII/>`__
-  `Geometrie - Clasa VII EDP 1995 <http://s.go.ro/f5wovuzo>`__
-  `Algebră - Clasa VIII EDP 1983 <http://manualul.info/Alg_VIII>`__
-  `Geometrie Clasa VIII EDP 1983 <http://manualul.info/Geom_VIII>`__

Manuale vechi liceu
-------------------

-  `Algebră - Clasa IX - 1996 Niță, Năstăsescu,
   Rizescu <http://manualul.info/TH/Algebra_IX_1996.pdf>`__
-  `Geometrie, Trigonometrie - Clasa IX EDP
   1980 <http://manualul.info/Geom_IX_1988/>`__
-  `Algebră - Clasa X EDP 1989 <http://manualul.info/Alg_X_1989/>`__
-  `Geometrie, Trigonometrie - Clasa X EDP
   1989 <http://manualul.info/Geom_X_1989>`__
-  `Matematică: Geometrie analitică, clasa a XI-a,
   1991 <http://manualul.info/Geom_XI_1985_Anal/Geom_XI_1991_Anal.pdf>`__
-  `Matematică: Elemente de algebră superioară (permutări, matrice,
   rang), clasa a XI-a, 1995
   local <http://manualul.info/TH/Elemente_de_Algebra_Superioara_XI_1995.pdf>`__
-  `Matematică: Elemente de analiză matematică, clasa a XI-a,
   1995 <http://manualul.info/TH/Elemente_de_Analiza_Matematica_XI_1995.pdf>`__
-  `Matematică: Algebră (Grupuri, Inele, Polinoame, Vectori), clasa a
   XII-a, 1989 <http://manualul.info/Algebra_XII_89>`__
-  `Matematică: Algebră (Grupuri, Inele, Polinoame, Vectori), clasa a
   XII-a, 1995 <http://manualul.info/TH/Algebra_XII_1995.pdf>`__
-  `Elemente de algebră superioară (Polinoame, Ecuaţii, Grupuri şi
   Inele), clasa a XII-a, 1977 <http://manualul.info/Alg_XII_1977>`__
-  `Matematică: Analiză, clasa a XII-a,
   1996 <http://manualul.info/TH/Elemente_de_Analiza_Matematica_XII_1996.pdf>`__
-  `Matematică: Elemente de teoria probabilităţilor şi statistică
   matematică, clasa a XII-a,
   1988 <http://manualul.info/Mate_XII_1988_Stat>`__
