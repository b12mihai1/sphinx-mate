.. Infty Mathematics documentation master file, created by
   sphinx-quickstart on Fri May  8 12:23:09 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Prima pagină
============================

.. figure:: images/17equations.jpg
   :alt: 17 equations that changed the world (Ian Stewart)
   :scale: 40

*Cele 17 ecuații care au schimbat lumea. Autor: Ian Stewart*

.. toctree::
   :maxdepth: 2
   :caption: Conținuturi:

   manuale
   materiale-58
   materiale-912
   ytvideos/rezvexamen
   ytvideos/telescoala
   ytvideos/khan
   programe
   bacalaureat
   subiecte


.. raw:: html
   <form action="https://www.paypal.com/donate" method="post" target="_top">
   <input type="hidden" name="hosted_button_id" value="QHGNESXAU9JDU" />
   <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
   <img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1" />
   </form>

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
