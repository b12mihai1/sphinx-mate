Subiecte date la bacalaureat
=============================

.. meta::
   :keywords lang=ro: bacalaureat programa subiecte culegere probleme variante bac


Am adunat aici o culegere ușor accesibilă cu subiectele care s-au dat la examenele de bacalaureat din anii trecuti: 

`Subecte Bacalaureat date intre anii 2007 si 2023 <https://s.go.ro/9inm6fi5>`__

În cadrul examenului național de bacalaureat Matematica are statut de disciplină obligatorie în
funcție de filieră, profil și specializare. Astfel, programele de examen se diferenţiază, în funcţie de filiera, profilul şi specializarea absolvite, în:
  - programa M_mate-info pentru filiera teoretică, profilul real, specializarea matematică-informatică şi pentru filiera vocaţională, profilul militar, specializarea matematică-informatică;
  - programa M_şt-nat pentru filiera teoretică, profilul real, specializarea ştiinţe ale naturii;
  - programa M_tehnologic pentru filiera tehnologică: profilul servicii, toate calificările profesionale; profilul resurse naturale şi protecţia mediului, toate calificările profesionale; profilul tehnic, toate calificările profesionale;
  - programa M_pedagogic pentru filiera vocaţională, profilul pedagogic, specializarea învăţător-educatoare


`Întreaga programa la bacalaureat MATEMATICA poate fi consultata aici <https://s.go.ro/4omkmnfc>`__
