Gimnaziu: Lecții
=================

.. figure:: images/math01.jpg
   :alt: Euclid's "Elements," written about 300 B.C
   :width: 500
   :height: 500

*Pagină din manuscrisul "Elemente" al lui Euclid, scris în anul 300 î.Hr. Sursa:* `ibiblio vatican <https://www.ibiblio.org/expo/vatican.exhibit/exhibit/d-mathematics/Greek_math.html>`__


Această pagină conține lecții pentru gimnaziu adunate din 3 mari surse:

 - materiale proprii
 - Teleșcoala TVR
 - Alte canale de youtube de matematica

`Culegere de matematică - Gheba - V-IX <http://s.go.ro/zgmsdd93>`__

Clasa V
-------

Numere naturale. Operații cu numerele naturale
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- `Playlist MATH-PDR: Scrierea și citirea numerelor naturale <https://www.youtube.com/playlist?list=PLZlP4x6DpFHubjz7tOrq8Syy4wI-FU9TB>`__
- `Playlist MATH-PDR: Ridicarea la putere a numerelor naturale <https://www.youtube.com/playlist?list=PLZlP4x6DpFHvPQ5RQjUlDZGouHWaVbeAk>`__
- `FIȘĂ RECAPITULATIVĂ: Operații cu numere naturale <http://s.go.ro/426ldtyt>`__ (sursa: M. Perianu, C. Stănică, I. Balica, C. Mîinescu, C. Lazăr - Matematică, Evaluarea Națională 2017, Clubul Matematicienilor, editura ART)

Metode aritmetice de rezolvare a problemelor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- `VIDEO: Aritmetică: Probleme de Organizare a Datelor <https://youtu.be/8uYoH2lQDHs>`__

Divizibilitatea numerelor naturale
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- `Playlist MATH-PDR: Divizibilitate <https://www.youtube.com/playlist?list=PLZlP4x6DpFHvxrgwFSejwsi98M1beju8->`__
- `TELEȘCOALA: Matematică - Recapitulare, divizibilitate <http://www.tvr.ro/tele-coala-matematica-a-viii-a-recapitulare-divizibilitate-video_28934.html>`__
- `TELEȘCOALA: Divizibilitate, recapitulare, tema 3 <https://www.youtube.com/watch?v=kdJgVDJXzVM>`__

Fracții ordinare și zecimale. Operații
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Această secțiune conține lecții din capitolele: Fracții ordinare, Operații cu fracții ordinare, Fracții zecimale, Operații cu fracții zecimale

**FIȘE**:

-  `Media aritmetică. Ridicarea la putere <http://s.go.ro/ffwucq28>`__
-  `Împărțirea fracțiilor zecimale <http://s.go.ro/9mda8m30>`__
-  `Ordinea efectuării operațiilor <http://s.go.ro/jjp7dd24>`__
-  `Rezolvarea problemelor cu fracții: Metode aritmetice de rezolvare a problemelor <http://s.go.ro/qc45ges1>`__

**LECȚII VIDEO**:

-  `Playlist MATH-PDR: <https://www.youtube.com/playlist?list=PLZlP4x6DpFHsxnIBJJCYKCy2hfBUGM7bZ>`__ - Se incepe lucrul cu numere rationale pozitive: fracţii: subunitare, echiunitare, supraunitare, fracţii echivalente, amplificarea şi simplificarea fracţiilor, aflarea unei fracţii dintr-un număr natural, procent, aproximări la ordinul zecimilor/sutimilor, reprezentarea pe axa  numerelor a fracţiilor zecimale, compararea şi ordonarea fracţiilor zecimale, adunarea fracţiilor zecimale care au un număr finit de zecimale nenule, scăderea fracţiilor zecimale care au un număr finit de zecimale nenule


Punct, dreaptă, plan
~~~~~~~~~~~~~~~~~~~~

-  `Punct, dreaptă, plan, semiplan, semidreaptă, segment de
   dreaptă <http://s.go.ro/w4v1e6ak>`__
-  `Pozițiile relative ale unui punct față de o dreaptă. Puncte
   coliniare <http://s.go.ro/mchfi62s>`__
-  **FIȘĂ**: `Pozițiile relative a două drepte <http://s.go.ro/2kpxldjl>`__
- **VIDEO**:  `Pozițiile relative a două drepte <https://www.youtube.com/watch?v=QnqIKw9X3S8>`__

-  `Lungimea unui segment. Distanța dintre două puncte. Segmente congruente <http://s.go.ro/ta8sctnl>`__
- **VIDEO**: `Lungimea unui segment <https://www.youtube.com/watch?v=nW8MITkgamM>`__

-  `Mijlocul unui segment Simetricul unui punct față de un punct <http://s.go.ro/63w0mcd2>`__
- **VIDEO**: `Mijlocul unui segment <https://www.youtube.com/watch?v=s_xn1IHWuOc>`__

Unghiul
~~~~~~~

-  **VIDEO**: `Unghiul <https://youtu.be/cy6WZ5PviL4>`__
-  `Unghi: definiție, notații, elemente. Interiorul unui unghi,
   exteriorul unui unghi. Măsurarea unui unghi. Operații cu măsuri de
   unghiuri. Unghiuri congruente. Clasificări <http://s.go.ro/gzk62ime>`__
-  `Clasificarea unghiurilor <http://s.go.ro/rsxrt5bi>`__
-  **VIDEO**: `Clasificarea unghiurilor <https://www.youtube.com/watch?v=lnfjZ4cxzcI>`__
-  **Temă**: `Calcule cu măsuri de unghiuri <http://s.go.ro/jjuwzvwp>`__
-  `Figuri congruente <http://s.go.ro/3s8ebc0k>`__


Unități de măsură
~~~~~~~~~~~~~~~~~

-  `Unități de măsură pentru lungime <http://s.go.ro/rzmdinaq>`__
-  `Perimetrul <http://s.go.ro/xyuvwvhb>`__
-  `Unități de măsură pentru suprafață. Transformări <http://s.go.ro/7st7mgwf>`__
-  `Unități de măsură pentru volum. Transformări <http://s.go.ro/jtfv0gqh>`__

Clasa VI
--------

Algebră
~~~~~~~

Mulțimi. Mulțimea numerelor naturale
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  `TELEȘCOALA: Mulţimi de numere - recapitulare <https://youtu.be/H916oxeRMlw>`__

Rapoarte și proporții
^^^^^^^^^^^^^^^^^^^^^

- `FIȘĂ Teorie rapoarte <http://s.go.ro/orrzd26g>`__

**VIDEO TELEŞCOALA TVR**:

- `TELEȘCOALA: Matematică, rapoarte și proporții <http://www.tvr.ro/tele-coala-matematica-a-viii-a-rapoarte-i-propor-ii-video_29027.html#view>`__
- `TELEȘCOALA: Rapoarte, proporții, procente <https://www.youtube.com/watch?v=sWkns0OH1NM>`__
- `TELEȘCOALA: Mărimi direct și invers proporționale. Probabilități <https://www.youtube.com/watch?v=sHFiNW9Ow_Q>`__

**Lecții video Prof. Radu Poenaru (MATH-PDR)**:

- `Playlist rapoarte și proporții <https://www.youtube.com/playlist?list=PLZlP4x6DpFHto_IVggFRt0FdoYPuzVymM>`__

Numere întregi
^^^^^^^^^^^^^^

-  `Ecuații cu numere întregi <http://s.go.ro/9aj03k5x>`__
-  `Inecuații cu numere întregi <http://s.go.ro/1bllzcja>`__
-  `Probleme - Ecuații cu numere întregi <http://s.go.ro/i28hxkmh>`__
-  `Probleme care se rezolvă cu ajutorul ecuațiilor și inecuațiilor <http://s.go.ro/ori0k6j5>`__
-  `VIDEO: Playlist MATH-PDR - Ecuații în Z <https://www.youtube.com/playlist?list=PLZlP4x6DpFHt4A0R8R5VuVwtMQ6fzoEpt>`__
-  `VIDEO: Playlist MATH-PDR - Inecuații în Z <https://www.youtube.com/playlist?list=PLZlP4x6DpFHvw3PeBFlRNTri1bRZ4ZgGy>`__


Mulțimea numerelor raționale
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  `Mulțimea numerelor raționale <http://s.go.ro/xb313fzl>`__
-  `Compararea numerelor raționale <http://s.go.ro/7rqddn3m>`__
-  `Adunarea și scăderea numerelor raționale <http://s.go.ro/mwhmagqi>`__
-  `Înmulțirea numerelor raționale <http://s.go.ro/n5oqzx2w>`__
-  `Ridicarea la putere numere raționale <http://s.go.ro/kd3cmx5a>`__
-  `Împărțirea numerelor raționale <http://s.go.ro/1y459y4i>`__
-  `Ordinea efectuării operațiilor cu numere raționale <http://s.go.ro/84ov89zj>`__
-  `Lecție: Ecuații cu numere raționale <http://s.go.ro/bwuosdbv>`__
-  `Exerciții rezolvate: Ecuații cu numere raționale <http://s.go.ro/73us8je8>`__
-  `Probleme: Ecuații cu numere raționale <http://s.go.ro/ajwlqxve>`__

Geometrie
~~~~~~~~~

Unghiuri
^^^^^^^^

**Lecții video Prof. Radu Poenaru (MATH-PDR)**:

- `Suma masurilor unghiurilor unui triunghi - problema rezolvata <https://www.youtube.com/watch?v=hD7xzvsZGHs>`__

**Lecții Khan Academy**:

- `Introduction to Angles | Angles and intersecting lines <https://www.youtube.com/watch?v=2439OIVBgPg>`__
- `Angle basics | Angles and intersecting lines <https://www.youtube.com/watch?v=H-de6Tkxej8>`__
- `Complementary and supplementary angles | Angles and intersecting lines <https://www.youtube.com/watch?v=BTnAlNSgNsY>`__
- `Acute right and obtuse angles | Angles and intersecting lines <https://www.youtube.com/watch?v=ALhv3Rlydig>`__
- `Measuring angles in degrees | Angles and intersecting lines <https://www.youtube.com/watch?v=92aLiyeQj0w>`__
- `Introduction to vertical angles | Angles and intersecting lines <https://www.youtube.com/watch?v=_7aUxFzTG5w>`__
- `Angles formed by parallel lines and transversals <https://www.youtube.com/watch?v=H-E5rlpCVu4>`__

Paralelism și perpendicularitate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Lecții video Prof. Radu Poenaru (MATH-PDR)**:

- `Drepte perpendiculare <https://www.youtube.com/playlist?list=PLZlP4x6DpFHu5ut1egN_OwPKIQtq6uyrR>`__
- `Criterii de paralelism, suma măsurilor unghiurilor unui triughi - problemă rezolvată <https://www.youtube.com/watch?v=4sRXeF0vJwc>`__

**Lecții Khan Academy**:

- `Parallel and perpendicular lines intro | Geometry | Khan Academy  <https://www.youtube.com/watch?v=V0xounKGEXs>`__
- `Identifying parallel and perpendicular lines | Geometry | Khan Academy <https://www.youtube.com/watch?v=aq_XL6FrmGs>`__
- `Proof: parallel lines have the same slope | Geometry | Khan Academy <https://www.youtube.com/watch?v=9hryH94KFJA>`__

Cercul
^^^^^^

-  `Lecție video cls 6 - prof. Radu Poenaru - Cercul: Arce de cerc și unghiuri la centru <https://www.youtube.com/watch?v=Bu1lnPlq-8A>`__
-  `Teorie - Cercul <http://s.go.ro/w3mozu3w>`__
-  `TELEȘCOALA: Matematică – Cercul - partea I <http://www.tvr.ro/tele-coala-matematica-a-viii-a-cercul-video_29143.html#view>`__
-  `TELEȘCOALA: Matematică – Cercul - partea II <http://www.tvr.ro/tele-coala-matematica-a-viii-a-cercul-partea-a-ii-a-video_29226.html#view>`__


Proprietățile triunghiurilor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Congruența triunghiurilor

-  **FIȘĂ TEORIE**: `Congruența triunghiurilor <http://s.go.ro/xg28jmk4>`__
-  **VIDEO**: `Congruența triunghiurilor <https://www.youtube.com/watch?v=XpoFNU_8i-g>`__
-  **TEMĂ**: `Congruența triunghiurilor <http://s.go.ro/hss0hjh5>`__

Congruența triunghiurilor drepunghice

-  **FIȘĂ TEORIE**: `Congruența triunghiurilor drepunghice <http://s.go.ro/vnylf4hf>`__
-  **Exerciții rezolvate**: `Congruența triunghiurilor dreptunghice <http://s.go.ro/599mp7d8>`__

Metoda Triunghiurilor Congruente

-  **FIȘĂ**: `Metoda Triunghiurilor Congruente <http://s.go.ro/3nyzo5o6>`__
-  **VIDEO**: `Metoda Triunghiurilor Congruente <https://www.youtube.com/watch?v=kzgVf70PDZU>`__

Proprietățile triunghiului isoscel

-  **FIȘĂ**: `Proprietățile triunghiului isoscel - partea 1 <http://s.go.ro/xyjyj97d>`__
-  **VIDEO**: `Proprietățile triunghiului isoscel - partea 1 <https://youtu.be/0SpQXXAXab8>`__

-  **FIȘĂ**: `Proprietățile triunghiului isoscel - partea 2 <http://s.go.ro/r86x784l>`__
-  **VIDEO**: `Proprietățile triunghiului isoscel - partea 2 <https://www.youtube.com/watch?v=_Q66FJWxQR8>`__

Proprietățile triunghiului echilateral

-  **FIȘĂ**:  `Proprietățile triunghiului echilateral <http://s.go.ro/afl7rmh3>`__
-  **VIDEO**: `Proprietățile Triunghiului Echilateral <https://www.youtube.com/watch?v=M0aBGv6NHUA>`__

Proprietățile triunghiului dreptunghic

-  **FIȘĂ**: `Proprietătile triunghiului dreptunghic <http://s.go.ro/wbeowpw7>`__
-  **VIDEO**: `Proprietățile Triunghiului Drepunghic <https://www.youtube.com/watch?v=qJ8V1wbAl8s>`__
-  **FIȘĂ**:  `Teorema lui Pitagora <http://s.go.ro/jrlncz2k>`__
-  **VIDEO**: `Teorema lui Pitagora <https://www.youtube.com/watch?v=8RZqtFxg3YI>`__

Lecții recapitulative TELEŞCOALA TVR:

- **TELEŞCOALA**: `Triunghiul partea 1 <https://www.youtube.com/watch?v=iNoYEKF_Txo>`__ 
- **TELEŞCOALA**: `Triunghiul partea 2 <https://www.youtube.com/watch?v=80Z3ZvUBluk>`__
- **TELEŞCOALA**: `Matematică – Linii importante în triunghi <http://www.tvr.ro/tele-coala-matematica-a-viii-a-linii-importante-in-triunghi-video_29061.html>`__
- **TELEŞCOALA**: `Matematică – Recapitulare triunghiul <http://www.tvr.ro/tele-coala-matematica-a-viii-a-recapitulare-triunghiul_28955.html#view>`__


Clasa VII
---------

.. _algebră-1:

Algebră
~~~~~~~

Mulțimea numerelor reale
^^^^^^^^^^^^^^^^^^^^^^^^

-  `Radicali (Rădăcina pătrată a unui număr
   real) <http://s.go.ro/rs4fjz5j>`__
-  `Reguli de calcul cu radicali - partea
   I <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/Regulicalculradicali.pdf?path=%2FRegulicalculradicali.pdf>`__
-  `Reguli de calcul cu radicali - partea a
   II-a <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/ReguliCalculRadicali2.pdf?path=%2FReguliCalculRadicali2.pdf>`__
-  `Reguli de calcul cu
   puteri <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/RegulidecalculPuteri.pdf?path=%2FRegulidecalculPuteri.pdf>`__
- `TELEȘCOALA TVR: Puteri de numere <https://www.youtube.com/watch?v=mswKZefnGd0>`__

-  `Numere
   iraționale <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/NumereIrationale.pdf?path=%2FNumereIrationale.pdf>`__
-  `Raționalizarea
   numitorilor <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/RationalizareaNumitorilor.pdf?path=%2FRationalizareaNumitorilor.pdf>`__
-  `Fișă de lucru - Radicali <http://s.go.ro/nwtsdulr>`__
-  `Modulul unui număr - Noțiuni
   introductive <http://s.go.ro/jtc6bykx>`__

- `TELEȘCOALA: Matematică Recapitulare operaţii cu numere reale <http://www.tvr.ro/telescoala-matematica-clasa-a-viii-a-recapitulare-operatii-cu-numere-reale-video_28944.html#view>`__

-  **VIDEO**: `Produsul cartezian a două mulțimi <https://www.youtube.com/watch?v=CSd6-sqdJfo>`__

Ecuații și sisteme de ecuații liniare
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  `Relația de egalitate în mulțimea numerelor reale <http://s.go.ro/xwh59zle>`__
-  `Ecuația de gradul I <http://s.go.ro/rm1z1tcr>`__
-  `Sisteme de ecuații <http://s.go.ro/ub9htwwh>`__. Credit: prof. Vlad Copil
-  **VIDEO**: `Rezolvare inecuații în mulțimea numerelor întregi <https://youtu.be/iT8Lv8DFkjE>`__

**Alte lecții Teleșcoala TVR**:

- `Sisteme de 2 ecuații liniare cu 2 necunoscute <http://www.tvr.ro/tele-coala-matematica-a-viii-a-sisteme-de-2-ecua-ii-liniare-cu-2-necunoscute-video_29074.html#view>`__
- `Sisteme de ecuații, metoda substituției <https://www.youtube.com/watch?v=1Y3lQAJzzAo>`__
- `Sisteme de ecuații, metoda reducerii <https://youtu.be/pNvveV03Vic>`__
- `Probleme rezolvate prin sisteme de ecuații <https://youtu.be/5Qk9vtI-5xc>`__
- `Inecuații de gradul I <https://www.youtube.com/watch?v=Jwnuo7RjMEc&feature=youtu.be>`__
- `Probleme rezolvate cu ajutorul ecuațiilor și al inecuațiilor <https://youtu.be/7FQFJA-tr9g>`__


Elemente de organizare a datelor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  `Elemente de organizare a datelor <http://s.go.ro/6tr57ot8>`__

.. _geometrie-1:

Geometrie
~~~~~~~~~

.. raw:: html

   <!-- * [Lecții Geometrie](http://s.go.ro/3thpursn) -->

Patrulatere
^^^^^^^^^^^

-  `Teorie Patrulatere <http://s.go.ro/deogfr70>`__
-  `TELEȘCOALA: Matematică, Recapitulare - Trapezul <http://www.tvr.ro/tele-coala-matematica-a-viii-a-recapitulare-trapezul-video_29068.html#view>`__
-  `TELEŞCOALA: Matematică – Paralelogramul şi dreptunghiul <http://www.tvr.ro/telescoala-matematica-a-viii-a-paralelogramul-si-dreptunghiul-video_29118.html#view>`__
-  `TELEŞCOALA: Patrulatere Recapitulare <https://www.youtube.com/watch?v=66Sw-g8ZsQQ>`__

-  `Linia mijlocie în triunghi <http://s.go.ro/71i2w3yn>`__
-  `Linia mijlocie în triunghi și trapez <http://s.go.ro/y7hyt3g6>`__ . Documentul original este
   `aici <https://dokumen.tips/documents/linia-mijlocie-in-triunghi-si-in-trapez321.html>`__
-  `Ariile triunghiurilor și ale
   patrulaterelor <http://s.go.ro/t1mgru3r>`__
-  `Exerciții - Arii <http://s.go.ro/xb43j7rw>`__

.. _cercul-1:

Cercul
^^^^^^

-  `Fișă Teorie - Cercul <http://s.go.ro/w3mozu3w>`__
-  `TELEȘCOALA: Matematică – Cercul - partea I <http://www.tvr.ro/tele-coala-matematica-a-viii-a-cercul-video_29143.html#view>`__
-  `TELEȘCOALA: Matematică – Cercul - partea II <http://www.tvr.ro/tele-coala-matematica-a-viii-a-cercul-partea-a-ii-a-video_29226.html#view>`__
-  `MATH-PDR (prof. Radu Poenaru) - Cercul - Lecții video - curs și aplicații <https://www.youtube.com/playlist?list=PLZlP4x6DpFHtLiBk2XwtxnZ-mknVB12BO>`__


Asemănarea triunghiurilor
^^^^^^^^^^^^^^^^^^^^^^^^^

-  `TELEȘCOALA: Asemnănarea triunghiurilor <https://www.youtube.com/watch?v=pFTVgAqio6A>`__
-  `TELEȘCOALA: Matematică - Asemănarea triunghiurilor <http://www.tvr.ro/tele-coala-matematica-a-viii-a-asemanarea-triunghiurilor-video_29159.html#view>`__

-  `Fișă Asemănarea triunghiurilor <http://s.go.ro/6bohiz5w>`__
-  `Teorie - Triunghiuri asemenea <http://s.go.ro/xtdx8kqw>`__
-  `Temă - Asemănarea triunghiurilor <http://s.go.ro/ki0jv0jv>`__


Relații metrice în triunghiul dreptunghic
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  `Proiecții ortogonale <http://s.go.ro/kkyspcyu>`__
-  `Teorema lui Pitagora <http://s.go.ro/dzlfzqs3>`__
-  `TELEȘCOALA: Matematică - Relaţii metrice în triunghiul dreptunghic <http://www.tvr.ro/tele-coala-matematica-a-viii-a-relatii-metrice-in-triunghiul-dreptunghic-video_29214.html#view>`__


Elemente de trigonometrie
^^^^^^^^^^^^^^^^^^^^^^^^^

Introducere în trigonometrie

-  **VIDEO**: `Noțiuni de trigonometrie <https://youtu.be/TG8H15WMNUY>`__
-  **FIȘĂ**: `Noțiuni de trigonometrie <http://s.go.ro/nhtlopqo>`__
-  `Aplicații trigonometrie <http://s.go.ro/l8ioobqs>`__
-  `Arii calculate folosind sinus <http://s.go.ro/7f5mya0n>`__
-  `Rezolvarea triunghiului dreptunghic <http://s.go.ro/mgc0d9xv>`__

Poligoane regulate

-  `Fișă teorie - Poligoane regulate <http://s.go.ro/774gqy5b>`__
-  `TELEȘCOALA: Matematică - Poligoane regulate. Triunghiul echilateral <http://www.tvr.ro/tele-coala-matematica-a-xii-a-poligoane-regulate-triunghiul-echilateral-video_29246.html#view>`__

-  **VIDEO**:  `Calculul elementelor în poligoane regulate <https://www.youtube.com/watch?v=X7_uK4iJ5Q0>`__
-  **FIȘĂ**:   `Calculul elementelor poligoanelor regulate <http://s.go.ro/iut5l3m8>`__

-  `TELEȘCOALA: Matematică - Poligoane regulate: Pătratul și hexagonul <http://www.tvr.ro/tele-coala-matematica-a-viii-a-poligoane-regulate-patratul-i-hexagonul-video_29263.html#view>`__


Clasa VIII
----------

.. _algebră-2:

Algebră
~~~~~~~

Mulțimi de numere reale. Intervale
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  `Numere
   reale <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/NumereReale.pdf?path=%2FNumereReale.pdf>`__
-  `Inegalități numere
   reale <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/InegalitatiNumereReale.pdf?path=%2FInegalitatiNumereReale.pdf>`__
-  `Intervale -
   Teorie <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/Intervaleteorie.pdf?path=%2FIntervaleteorie.pdf>`__
-  `Modele teste scurte
   intervale <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/ModeleTesteScurteIntervale.pdf?path=%2FModeleTesteScurteIntervale.pdf>`__

**Lecții VIDEO Teleșcoala TVR**:

   - `Intervale de numere reale <https://youtu.be/KFB1XMz4vrw>`__
   - `Operaţii cu intervale de numere reale <https://youtu.be/cG8BewhzvLA>`__


Reguli de calcul în mulțimea numerelor reale
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  `Reguli de calcul cu radicali - partea
   I <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/Regulicalculradicali.pdf?path=%2FRegulicalculradicali.pdf>`__
-  `Reguli de calcul cu radicali - partea a
   II-a <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/ReguliCalculRadicali2.pdf?path=%2FReguliCalculRadicali2.pdf>`__
-  `Reguli de calcul cu
   puteri <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/RegulidecalculPuteri.pdf?path=%2FRegulidecalculPuteri.pdf>`__
-  `Numere
   iraționale <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/NumereIrationale.pdf?path=%2FNumereIrationale.pdf>`__
-  `Raționalizarea
   numitorilor <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/RationalizareaNumitorilor.pdf?path=%2FRationalizareaNumitorilor.pdf>`__
-  `Exerciții dificile - Raționalizarea
   numitorilor <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/ExercitiiDificileRationalizareaNumitorilor.pdf?path=%2FExercitiiDificileRationalizareaNumitorilor.pdf>`__
-  `Fișă de lucru -
   Radicali <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/FisaLucruRadicali.pdf?path=%2FFisaLucruRadicali.pdf>`__

Calcule cu numere reale
^^^^^^^^^^^^^^^^^^^^^^^

-  `Formule de calcul prescurtat <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/Formulecalculprescurtat.pdf?path=%2FFormulecalculprescurtat.pdf>`__
-  `Exerciții propuse: formule de calcul prescurtat <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/FormuleCalculPrescurtatExercitiiPropuse.pdf?path=%2FFormuleCalculPrescurtatExercitiiPropuse.pdf>`__
-  `Descompunere în factori <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/Descompunereafactorilor.pdf?path=%2FDescompunereafactorilor.pdf>`__
-  `Exerciții rezolvate: descompunerea termenilor <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/ExercitiiRezolvateDescompunereatermenilor.pdf?path=%2FExercitiiRezolvateDescompunereatermenilor.pdf>`__
-  `Maxime și minime. Inegalități algebrice <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/InegalitatiAlgebrice.pdf?path=%2FInegalitatiAlgebrice.pdf>`__

**LECȚII VIDEO TELEŞCOALA TVR**

-  `TELEȘCOALA: Matematică, a VIII-a – Formule de calcul prescurtat <https://youtu.be/kEi-tVtt3aI>`__
-  `TELEȘCOALA: Matematică, a VIII-a, Aplicații la formulele de calcul prescurtat <https://youtu.be/-eQaCegLoXs>`__
-  `TELEȘCOALA: Matematică, a VIII–a - Descompunerea în factori <https://youtu.be/iuW6TuFZvQA>`__
-  `TELEȘCOALA: Matematică, a VIII-a - Metode de descompunere în factori <https://youtu.be/IoyBcUrLePc>`__

Rapoarte de numere reale
^^^^^^^^^^^^^^^^^^^^^^^^

-  `Conspect: rapoarte de numere
   reale <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/ConspectRapoartedenrreale.pdf?path=%2FConspectRapoartedenrreale.pdf>`__
-  `Exerciții: rapoarte de numere reale reprezentate prin
   litere <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/ExercitiiRapoarteAlgebrice1.pdf?path=%2FExercitiiRapoarteAlgebrice1.pdf>`__

- **VIDEO TELEŞCOALA TVR**: `Operații cu fracții algebrice 1 <https://youtu.be/-m4dPRz2CSc>`__
- **VIDEO TELEŞCOALA TVR**: `Operații cu fracții algebrice 2 <https://youtu.be/Pow2-935sYQ>`__

Funcții
^^^^^^^

-  `Funcții – Noțiuni
   introductive <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/Functii.pdf?path=%2FFunctii.pdf>`__

**LECȚII VIDEO TELEŞCOALA**:

- `Matematică, a VIII-a - Funcţii definite pe mulţimi finite (I) <https://youtu.be/i1cDDZZ5vjo>`__
- `Matematică, a VIII-a - Funcţii definite pe mulţimi finite (II) <https://youtu.be/S20aRzzVuuQ>`__
- `Matematică, a VIII-a - Algebră, funcția de forma f: R→R, intersecția cu axele de coordonate <https://youtu.be/5DdsZWCWQ7A>`__
- `Matematică, a VIII-a - Funcţia liniară. Aplicaţii (I) <https://youtu.be/6xEgaj22WuE>`__
- `Matematică, a VIII-a - Funcţia liniară. Aplicaţii (II) <https://youtu.be/X7CDxxTAkPc>`__


Ecuații. Inecuații
^^^^^^^^^^^^^^^^^^

-  `Inecuații în mulțimea numerelor
   reale <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/InecuatiiNumereReale.pdf?path=%2FInecuatiiNumereReale.pdf>`__
-  `Exerciții inițiere: Inecuații în mulțimea numerelor
   reale <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/TemaInitiereInecuatii.pdf?path=%2FTemaInitiereInecuatii.pdf>`__
-  `Exerciții aprofundare: Inecuații în mulțimea numerelor
   reale <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/ExercitiiRezolvateInecuatiiConsolidare.pdf?path=%2FExercitiiRezolvateInecuatiiConsolidare.pdf>`__

**LECȚII VIDEO TELEŞCOALA**:

- `TELEȘCOALA: Matematică, a VIII-a - Ecuații și inecuații <http://www.tvr.ro/tele-coala-matematica-a-viii-a-ecua-ii-i-inecua-ii-video_28989.html#view>`__
- `TELEȘCOALA: Matematică, a VIII-a - Rezolvarea problemelor cu ajutorul ecuațiilor <http://www.tvr.ro/tele-coala-matematica-a-viii-a-rezolvarea-problemelor-cu-ajutorul-ecua-iilor-video_29134.html#view>`__
- `TELEȘCOALA: Matematică, a VIII-a - Inecuații de gradul I <http://www.tvr.ro/tele-coala-matematica-a-viii-a-inecua-ii-de-gradul-i-video_29782.html#view>`__

Ecuația de gradul al doilea
^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Fișă**:


-  `Ecuația de gradul al doilea <https://storage.rcs-rds.ro/content/links/cf2e600b-d561-4c3b-a7cf-92f1708f5da5/files/get/Ecuatiadegradulaldoilea.pdf?path=%2FEcuatiadegradulaldoilea.pdf>`__

**LECȚII VIDEO TELEŞCOALA**:

- `Ecuația de gradul al doilea (I) <https://youtu.be/H3MwaD8SIKA>`__
- `Ecuația de gradul al doilea (II) <https://youtu.be/xnsGhpElcnM>`__

**LECȚII SUPLIMENTARE**:


-  `New way to solve quadratic equations <https://www.youtube.com/watch?v=oWFRU-ula-A&feature=youtu.be>`__ - acest videoclip prezintă o metodă diferită de a rezolva \
   ecuația de gradul al doilea folosind `relațiile lui Viete <https://liceunet.ro/ghid-polinoame/relatiile-lui-viete>`__  \
   și descompunerea ecuației, evitând calculul discriminantului. Merită urmărit dacă considerați această metodă mai simplă. \
   Varianta completă se găsește într-o lecție de 40 de minute făcută de prof. Po-Shen Loh: \
   `Examples: A Different Way to Solve Quadratic Equations <https://www.youtube.com/watch?v=XKBX0r3J-9Y>`__


Teste algebră
^^^^^^^^^^^^^^

-  `TELEȘCOALA: Matematică, a VIII-a – Test la algebră, model pentru Evaluarea Naţională <https://www.youtube.com/watch?v=NmAxbT6pUtE>`__ -  prof. Anca Toşa a încheiat \
   lecțiile de recapitulare la algebră și a rezolvat, împreună cu elevii de clasa a VIII-a, \
   exercițiile din modelul pentru Evaluarea Naţională propus de Ministerul Educaţiei. 

-  `TELEȘCOALA: Matematică, a VIII-a - Descompunerea expresiilor algebrice <https://youtu.be/PO3IxgxsWj8>`__ - prof. Cătălina Bărboiu a prezentat o lecție despre descompunerea expresiilor algebrice. 

.. _geometrie-2:

Geometrie
~~~~~~~~~

Recapitulare geometrie plană
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  `TELEȘCOALA: Matematică, a VIII-a - Geometrie plană, recapitulare <https://www.youtube.com/watch?v=Ab_qddXoAFk>`__ - prof. Carmen Avganți a recapitulat cu elevii de clasa a VIII-a cunoștințele de geometrie plană și a rezolvat subiectele din cadrul modelului de Evaluare Națională. Problemele de geometrie sunt la subiectul al doilea și la subiectul al treilea. 

Puncte. Drepte. Plane
^^^^^^^^^^^^^^^^^^^^^

-  `Determinarea
   planului <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/DeterminareaPlanuluiExercitii.pdf?path=%2FDeterminareaPlanuluiExercitii.pdf>`__
-  `Piramida: descriere și
   reprezentare <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/PiramidaSem1.pdf?path=%2FPiramidaSem1.pdf>`__
-  `Prisma: descriere și
   reprezentare <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/PrismateorieS1.pdf?path=%2FPrismateorieS1.pdf>`__
-  `Poziții relative ale unei drepte față de un plan - partea
   I <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/Pozitiirelativedouadr.pdf?path=%2FPozitiirelativedouadr.pdf>`__
-  `Poziții relative ale unei drepte față de un plan - partea a
   II-a <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/PozitiileRelativealeuneidreptefatdeplan.pdf?path=%2FPozitiileRelativealeuneidreptefatdeplan.pdf>`__
-  `Relația de paralelism între două drepte în
   spațiu <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/DrepteParaleleSpatiuTeorieExercitii.pdf?path=%2FDrepteParaleleSpatiuTeorieExercitii.pdf>`__
-  `Unghiul a două drepte în spațiu - partea
   I <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/Unghiula2drepteinspatiu.pdf?path=%2FUnghiula2drepteinspatiu.pdf>`__
-  `Unghiul a două drepte în spațiu - partea a
   II-a <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/Unghiula2drepteinspatiuPartea2.pdf?path=%2FUnghiula2drepteinspatiuPartea2.pdf>`__

- **VIEDO TELEŞCOALA TVR**: `Paralelism. Unghiul a două drepte <https://youtu.be/v9YjSyvLI9k>`__

Relații între puncte, drepte, plane
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  `Dreapta perpendiculară pe un plan. Distanța de la un punct la un
   plan <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/DreaptaPerpendicularaPlan.pdf?path=%2FDreaptaPerpendicularaPlan.pdf>`__
-  `Înălțimea și apotema
   piramidei <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/InaltimeaPiramidei.pdf?path=%2FInaltimeaPiramidei.pdf>`__
-  `Piramida
   regulată <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/PiramidaTrunchi.pdf?path=%2FPiramidaTrunchi.pdf>`__
-  `Recapitulare capitolul I
   geometrie <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/Recapitularecap1geometrie.pdf?path=%2FRecapitularecap1geometrie.pdf>`__
-  `Recapitulare test capitolul I
   geometrie <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/RecapitulareTestCapitolul1Geometrie.pdf?path=%2FRecapitulareTestCapitolul1Geometrie.pdf>`__

- **VIDEO TELEŞCOALA TVR**: `Perpendicularitatea <https://youtu.be/LYrA6S9jMfQ>`__
- **VIDEO TELEŞCOALA TVR**: `Dreapta perpendiculară pe un plan <https://youtu.be/nmOshQLAY8Y>`__
- **VIDEO TELEŞCOALA TVR**: `Distanța de la un punct la un plan <https://youtu.be/vlC59cLOoWQ>`__


Proiecții ortogonale pe plan
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  `Teorema celor trei
   perpendiculare <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/Teoremacelor3perpendiculare.pdf?path=%2FTeoremacelor3perpendiculare.pdf>`__
-  `Unghi
   diedru <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/Unghidiedru.pdf?path=%2FUnghidiedru.pdf>`__

Prisma
^^^^^^

-  `Lecție completă:
   Prisma <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/TeoriePrisma%20VARIANTANOUA.pdf?path=%2FTeoriePrisma%20VARIANTANOUA.pdf>`__
-  `Prisma
   regulată <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/Prismaregulata.pdf?path=%2FPrismaregulata.pdf>`__
-  `Cub - Exerciții
   1 <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/CubExercitii.pdf?path=%2FCubExercitii.pdf>`__
-  `Cub - Exerciții
   2 <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/2CUBexercitiisem2.pdf?path=%2F2CUBexercitiisem2.pdf>`__
-  `Recapitulare arii și
   volume <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/Recapitularetestsem2prisme.pdf?path=%2FRecapitularetestsem2prisme.pdf>`__

**Lecții video Math-PDR** (credit: prof. Radu Poenaru):

  - `Prisma: prisma triunghiulara, patrulatera, prisme drepte si prisme regulate <https://www.youtube.com/watch?v=EVcspvDstqE>`__
  - `Prisma triunghiulara regulata, problema rezolvata (8e11) <https://www.youtube.com/watch?v=6GDrjsipQSA>`__
  - `Prisma triunghiulara regulata - Problema rezolvata <https://www.youtube.com/watch?v=xI5pXuuGZBU>`__
  - `Prisma triunghiulara regulata, probleme rezolvate (8g24) <https://www.youtube.com/watch?v=IicyYTIwcNU>`__
  - `Cubul: Aria totala, volumul si lungimea diagonalei <https://www.youtube.com/watch?v=GxMxlHoKu6>`__

**Lecții video TELEŞCOALA**

  - `Prisma dreaptă <https://youtu.be/D-1U7L6Gkcg>`__
  - `Arie și volum prismă triunghiulară <https://youtu.be/qHlnVJHqZHM>`__
  - `Arie și volum prismă patrulateră <https://youtu.be/2fSE4-sxgk8>`__
  - `Paralelipipedul dreptunghic <https://youtu.be/3yKHYrhr1K8>`__
  - `Arie și volum Paralelipipedul dreptunghic <https://youtu.be/N3KbfAcHm6k>`__
  - `Cubul <https://youtu.be/-Vto-I7Tmsc>`__

Piramida
^^^^^^^^

-  `Piramida regulată - formule
   utile <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/Piramidaregulataformule.pdf?path=%2FPiramidaregulataformule.pdf>`__
-  `Exerciții piramida patrulateră
   regulată <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/ExercitiiPiramidaPatrulera.pdf?path=%2FExercitiiPiramidaPatrulera.pdf>`__
-  `Trunchi de piramidă
   regulată <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/TrunchiPiramida2.pdf?path=%2FTrunchiPiramida2.pdf>`__
-  `Trunchi de piramidă regulată -
   formule <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/TrunchiulPiramidaRegulata.pdf?path=%2FTrunchiulPiramidaRegulata.pdf>`__
-  `Trunchi de piramidă patrulateră -
   formule <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/FormuleTrunchiulPiramidaPatrulatera.pdf?path=%2FFormuleTrunchiulPiramidaPatrulatera.pdf>`__
-  `Exerciții rezolvate - Trunchi de piramidă
   patrulateră <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/ExercitiiRezolvateTrunchiulPiramidaPatrulatera.pdf?path=%2FExercitiiRezolvateTrunchiulPiramidaPatrulatera.pdf>`__
-  `Exerciții rezolvate - Trunchi de piramidă
   triunghiulară <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/LectieExercitiiRezolvateTrunchiPiramidaTriunghiulara.pdf?path=%2FLectieExercitiiRezolvateTrunchiPiramidaTriunghiulara.pdf>`__

**Lecții TELEŞCOALA TVR**:

-  `Piramida triunghiulară regulată 1 <https://youtu.be/yBbv9DrtV9s>`__
-  `Piramida triunghiulară regulată 2 <https://youtu.be/YYs2wTsKVbQ>`__
-  `Piramida patrulateră regulată <https://youtu.be/PpHKBcuS6WA>`__
-  `Piramida hexagonală regulată <https://youtu.be/7zYdfaxrXjc>`__
-  `Piramida hexagonală regulată (2021) <https://youtu.be/8ntnOlJQ1mQ>`__
-  `Piramida patrulateră, piramida hexagonală <https://youtu.be/6YqAF9_C2LM>`__
-  `Trunchiul de piramidă patrulateră regulată <https://youtu.be/NC8rc4njdZY>`__
-  `Trunchiul de piramidă patrulateră regulată (2021) <https://youtu.be/i3yyR1uFaSA>`__
-  `Trunchi de piramidă <https://youtu.be/cOh54_ObDIQ>`__
-  `Trunchi de piramidă hexagonală regulată <https://www.youtube.com/watch?v=a23Ikh9s8AE>`__

Corpuri rotunde
^^^^^^^^^^^^^^^

-  `Corpuri
   rotunde <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/CorpuriRotunde.pdf?path=%2FCorpuriRotunde.pdf>`__
-  `Formule utile corpuri rotunde -
   rezumat <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/RezumatFormuleCorpuriRotunde.pdf?path=%2FRezumatFormuleCorpuriRotunde.pdf>`__

-  **FIȘĂ**: `Cilindrul <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/CilindruLectieScoala.pdf?path=%2FCilindruLectieScoala.pdf>`__
-  **VIDEO**: `Cilindrul - Lecție și aplicații <https://www.youtube.com/watch?v=9_-1FbUvdIs>`__
- **TELEŞCOALA TVR**: `Cilindrul drept <https://youtu.be/_6jVnYy_7V4>`__
- **TELEŞCOALA TVR 2021**: `Matematică, a VIII-a - Cilindrul. Aplicații <https://youtu.be/sOl9G4-Q5cE>`__
- **TELEŞCOALA TVR 2021**: `Matematică, a VIII-a - Aria și volumul cilindrului <https://youtu.be/bqWnHhJJcNo>`__

-  `Conul <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/LectieConScoala.pdf?path=%2FLectieConScoala.pdf>`__
-  **TELEŞCOALA TVR**: `Conul circular drept <https://youtu.be/99J_tLKmnPs>`__
-  **TELEŞCOALA TVR**: `Trunchiul de con circular drept <https://youtu.be/wMlseq96LhM>`__
-  **TELEŞCOALA TVR**: `Sfera <https://www.youtube.com/watch?v=p6gXSNtAVsA>`__
-  **TELEŞCOALA TVR**: `Cilindrul circular drept. Conul circular drept <https://youtu.be/8ZOi129TgPs>`__
-  `Exerciții corpuri
   rotunde <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/Teorie.Exercitiirezolvate.CorpuriRotunde.pdf?path=%2FTeorie.Exercitiirezolvate.CorpuriRotunde.pdf>`__
-  `Suprafețe și corpuri rotunde - lecție completă (din manual
   vechi) <https://storage.rcs-rds.ro/content/links/9775c08c-f6cd-4718-b1cf-7ad560237851/files/get/CorpuriRotundeLectieCompleta.pdf?path=%2FCorpuriRotundeLectieCompleta.pdf>`__

Recapitulare examen Evaluare Națională (E.N)
--------------------------------------------

- **TELEŞCOALA TVR**: `Secțiuni în corpurile geometrice studiate <https://youtu.be/VJCzuttOOVc>`__
- **TELEŞCOALA TVR**: `Elemente de statistică matematică <https://youtu.be/DBg4r4U5OHM>`__


-  `Recapitulare teză semestrul 1 <http://s.go.ro/wft9z17u>`__

Materiale recapitulative pentru examenul de evaluare națională (E.N):

-  `ALGEBRĂ Breviar teoretic cu exemple concrete pentru pregătirea
   examenului de evaluare națională 2010
   (mateinfo.ro) <https://mateinfo.ro/teorie-formule-matematica/teorie-complexa-gimnaziu/teorie-matematica-gimnaziu/25-algebra-breviar-teoretic-cu-exemple-concrete-pentru-evaluarea-nationala-la-matematica>`__
   - backup local - `download
   link <https://storage.rcs-rds.ro/links/224a7964-99e0-492f-9ab4-4b413416ad7e>`__
-  `Formule poliedre și corpuri rotunde - 1 pagină
   (mateinfo.ro) <http://www.mateinfo.ro/teorie-formule-matematica/teorie-complexa-gimnaziu/teorie-matematica-gimnaziu/419-formule-poliedre-si-corpuri-rotunde-pe-o-pagina>`__
   - backup local - `download link <http://s.go.ro/43b4tt8c>`__
-  `Formule corpuri - poliedre și corpuri
   rotunde <https://mateinfo.ro/teorie-formule-matematica/teorie-complexa-gimnaziu/teorie-matematica-gimnaziu/26-formule-corpuri-poliedre-si-corpuri-rotunde>`__
   - backup local - `download link <http://s.go.ro/5xi9o0py>`__
-  `GEOMETRIE Breviar teoretic cu exemple concrete pentru pregătirea
   examenului de evaluare națională 2010
   (mateinfo.ro) <https://www.mateinfo.ro/teorie-formule-matematica/teorie-complexa-gimnaziu/teorie-matematica-gimnaziu/24-geometrie-breviar-teoretic/file>`__
   - backup local - `download link <http://s.go.ro/kmjtpmi2>`__
-  `Teorie geometrie plană pentru Evaluarea Națională la Matematică -
   partea I
   (mateinfo.ro) <https://mateinfo.ro/teorie-formule-matematica/teorie-complexa-gimnaziu/teorie-matematica-gimnaziu/28-teorie-geometrie-plana>`__
   - backup local - `download link <http://s.go.ro/v2aqu0c5>`__
-  `Formule geometria plană pe 1 pagină
   (mateinfo.ro) <https://mateinfo.ro/teorie-formule-matematica/teorie-complexa-gimnaziu/teorie-matematica-gimnaziu/364-geometria-plana-pe-o-pagina>`__
   - backup local - `download link <http://s.go.ro/u9m7yd6d>`__

Teste și modele EN
------------------

-  `Subiecte Evaluare Națională 2018 <http://subiecte2018.edu.ro/2018/evaluarenationala/index.html>`__
-  `Subiecte Evaluare Națională 2019 <http://subiecte2019.edu.ro/2019/evaluarenationala/index.html>`__
-  `Subiecte Evaluare Națională 2020 <http://subiecte2020.edu.ro/2020/evaluarenationala/index.html>`__


Teste antrenament 2020
~~~~~~~~~~~~~~~~~~~~~~~

-  `Teste de antrenament 2020 (subiecte și rezolvări scrise) <http://s.go.ro/pm62z7cj>`__

**TELEŞCOALA TVR - Rezolvări teste antrenament 2021**:

-  `Test de antrenament 1 - Subiectele 1 și
   2 <https://www.youtube.com/watch?v=DJa9SkMa8sE&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=113&t=0s>`__
-  `Test de antrenament 1 - Subiectul
   3 <https://www.youtube.com/watch?v=A8tRNt1toYo&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=117&t=0s>`__
-  `Test de antrenament 2 - Subiectele 1 și
   2 <https://www.youtube.com/watch?v=GHjtp4nUNUI&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=122&t=0s>`__
-  `Test de antrenament 2 - subiectul
   3 <https://www.youtube.com/watch?v=NJU4ZJG9GNc&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=126&t=0s>`__
-  `Test de antrenament 3 - subiectele 1 și
   2 <https://www.youtube.com/watch?v=ZxGCItGOkg4&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=130&t=0s>`__
-  `Test de antrenament 3 - subiectul
   3 <https://www.youtube.com/watch?v=b7NNe9uNhdM&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=134&t=0s>`__
-  `Test de antrenament 4 - subiectele 1 și
   2 <https://www.youtube.com/watch?v=KXu-jgmae28&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=141&t=0s>`__
-  `Test de antrenament 4 - subiectul
   3 <https://www.youtube.com/watch?v=3qGM0NRJsO4&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=145&t=0s>`__
-  `Test de antrenament 6 - subiectele 1 și
   2 <https://www.youtube.com/watch?v=CIu6rNfZdmo&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=149&t=0s>`__
-  `Test de antrenament 6 - subiectul
   3 <https://www.youtube.com/watch?v=wTS6PvcTaHs&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=154&t=0s>`__
-  `Test de antrenament 7 - subiectul
   3 <https://www.youtube.com/watch?v=Gan1PIlbIEY&list=PLxO8-C91Lp92xRiauh3jJUZxl2e32QTsU&index=157&t=0s>`__
-  `Test de antrenament 8 - subiectul 1, 2, 3 <https://youtu.be/hlhPZXXuqyw>`__
-  `Test de antrenament 9 - subiectul 1, 2, 3 <https://youtu.be/9PzxG_HQ37o>`__
-  `Test de antrenament 10 - subiectul 1, 2, 3 <https://youtu.be/dELvMGlM1AA>`__
-  `Test de antrenament 11 - subiectul 1, 2, 3 <https://youtu.be/-vx52KrYLIk>`__
-  `Test de antrenament 12 - subiectul 1, 2, 3 <https://youtu.be/droqwF0lTtM>`__
-  `Test de antrenament 13 - subiectul 1, 2, 3 <https://youtu.be/PbRXRKS5PwY>`__
-  `Test de antrenament 14 - subiectul 1, 2, 3 <https://youtu.be/-F1ulDYoAwA>`__
-  `Test de antrenament 16 - subiectul 1, 2, 3 <https://youtu.be/9KIs-XJdi0s>`__
-  `Test de antrenament 18 - subiectul 1, 2, 3 <https://youtu.be/BNtCYBoyHco>`__
-  `Test de antrenament model 18 - subiectul 1, 2, 3 <https://youtu.be/BVamcd2csf0>`__
-  `Test de antrenament model 19 - subiectul 1, 2, 3 <https://youtu.be/a17uzIbxbQA>`__
-  `Test de antrenament model 20 - subiectul 1, 2, 3 <https://www.youtube.com/watch?v=Aew2PxaNmQI>`__

Teste antrenament 2021
~~~~~~~~~~~~~~~~~~~~~~~

-  `Teste de antrennament 2021 (clonă rocnee) <http://s.go.ro/xnsqw43j>`__

**TELEŞCOALA TVR - Rezolvări teste antrenament 2021**:

Testul de antrenament nr. 1

- `Matematică, a VIII-a - Test de antrenament nr. 1/2021. Probleme de geometrie <https://youtu.be/mBWRpvnjpbs>`__
- `Matematică, a VIII-a - Test de antrenament nr. 1/2021. Probleme de algebră <https://youtu.be/Zk4cYhFu0ds>`__


Testul de antrenament nr. 2

- `Matematică, a VIII-a - Test antrenament nr.2. Subiectele I şi II <https://youtu.be/kVh4c1LkZbM>`__
- `Matematică, a VIII-a - Test antrenament nr.2, subiect III. Geometrie <https://youtu.be/klc0cMYvLyA>`__
- `Matematică, a VIII-a - Test de antrenament nr. 2, subiectul III (algebră) <https://youtu.be/z9u1ttnOAtI>`__

Testul de antrenament nr. 3

- `Matematică, a VIII-a - Testul de antrenament nr. 3/2021 <https://youtu.be/K4aWLzUw5PM>`__
- `Matematică, a VIII-a - Testul de antrenament nr. 3 (subiectul nr. 3, geometrie) <https://youtu.be/m9Cq18Y4Q6Q>`__
- `Matematică, a VIII-a - Testul de antrenament 3, subiectul III (algebră) <https://youtu.be/9PbfcMKK-Dw>`__

Testul de antrenament nr. 4

- `Matematică, a VIII-a - Testul de antrenament nr. 4, subiectele I și II - Evaluarea Națională <https://youtu.be/yzIMw_juklo>`__
- `Matematică, a VIII-a - Test antrenament nr 4. Subiect III. Algebra <https://youtu.be/fmA1yIWajcU>`__
- `Matematică, a VIII-a - Test de antrenament nr.4. Subiect III Geometrie <https://youtu.be/R2IA49eug4E>`__


Testul de antrenament nr. 5

-  `Matematică, a VIII-a - Rezolvarea subiectelor I și II din testul de antrenament nr. 5 <https://youtu.be/Nf-NgWP38ao>`__
-  `Matematică, a VIII-a - Test antrenament nr. 5. Subiect III. Geometrie <https://youtu.be/iY-n-aqAego>`__
-  `Matematică, a VIII-a - Test antrenament nr. 5. Subiect III. Algebră <https://youtu.be/I6KFjWuQ3O0>`__

Testul de antrenament nr. 6

- `Matematică, a VIII-a - Test antrenament nr. 6. Subiectele I şi II <https://youtu.be/GZeJhhTT1bY>`__
 
