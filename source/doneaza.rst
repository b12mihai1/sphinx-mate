Doneaza
========

Dacă ai găsit utile materialele site-ului
-----------------------------------------

PayPal
------

.. raw:: html

    <form action="https://www.paypal.com/donate" method="post" target="_top"><input type="hidden" name="hosted_button_id" value="QHGNESXAU9JDU" /><input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" /><img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1" /></form>

^

Revolut
-------

**RON**: RO92BREL0005501554950100

**EUR**: LT963250094171177156 (BIC REVOLT21)
