Liceu: Lecții
=============

.. figure:: images/math05.jpg
   :alt: Ottob. lat. 1850 fols. 36 verso - 37 recto math05 NS.52
   :width: 500
   :height: 500

*Pagină din manuscrisul "Lucrări" al lui Arhimede, Sursa:* `ibiblio vatican <https://www.ibiblio.org/expo/vatican.exhibit/exhibit/d-mathematics/Greek_math.html>`__


Clasa IX
--------

-  `Breviar teoretic <http://s.go.ro/q1pfrpyf>`__
-  `Inducția matematică <http://s.go.ro/08qbtt9n>`__
-  `Ecuația de gradul al doilea - Relațiile lui Viete <http://s.go.ro/7vvh5uaq>`__
-  `The simpler Quadratic formula | Lockdown Math ep. 1 <https://www.youtube.com/watch?v=MHXO86wKeDY>`__
-  `Trigonometry Fundamentals | Lockdown Math ep. 2 <https://www.youtube.com/watch?v=yBw67Fb31Cs>`__
-  `Tips to be a better problem solver [Last lecture] | Lockdown math ep. 10 <https://www.youtube.com/watch?v=QvuQH4_05LI>`__
-  `Playlist MATH-PDR (Prof Radu Poenaru) | Inducția matematică <https://www.youtube.com/playlist?list=PLZlP4x6DpFHtYjXaFbxDOdTktWJtZZa_L>`__

Probleme BAC
~~~~~~~~~~~~

-  `Subiectul 1 - Bacalaureat 2020 <https://1drv.ms/b/s!Ao4rAdqiT2b_hph2wma54ic2hsu84Q?e=VHXSdr>`__

Manuale și culegeri
~~~~~~~~~~~~~~~~~~~

-  `Gheba - Culegere de matematica V-IX <http://s.go.ro/zgmsdd93>`__
-  `Burtea - Culegere de matematica clasa a IX-a <https://1drv.ms/b/s!Ao4rAdqiT2b_hph0aZNf6LNB9rq8nQ?e=6UId6y>`__
-  `Burtea - Culegere de matematica clasa a X-a <https://1drv.ms/b/s!Ao4rAdqiT2b_hph1nIYz-trzk-1B0A?e=ArClQN>`__
