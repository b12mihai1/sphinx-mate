Programe școlare
================

.. figure:: images/earth.jpg
   :alt: Old Earth Map
   :width: 250
   :height: 250


Gimnaziu
--------

-  `Programa școlară gimnaziu <http://s.go.ro/x5isl4ns>`__

Evaluarea națională
-------------------

-  `Anexă cu programa la matematică pentru Evaluarea Națională 2020-2021
   a elevilor de clasa a VIII-a <http://s.go.ro/jrffo843>`__
-  `Ordinul nr. 3472/10.03.2020 privind aprobarea programelor pentru
   susținerea Evaluării Naționale pentru absolvenții clasei a VIII-a,
   începând cu anul școlar
   2020-2021 <https://rocnee.eu/evaluarenationala/20-03-2020-omec-en8.html>`__
   - `Link clonă locală <http://s.go.ro/fcje9y5q>`__

Liceu
-----

-  `Programa școlară - clasa a IX-a <http://s.go.ro/6pk70zw0>`__
