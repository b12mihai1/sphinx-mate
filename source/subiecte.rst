Subiecte admitere politehnica
=============================

 -  `Subiecte date la admitere Politehnica Bucuresti 2002-2022 (simulari si examen de admitere) <https://s.go.ro/jm617vod>`__
 -  `Subiecte EDU <http://subiecte.edu.ro/2022/>`__
 -  `Culegere de probleme admitere Politehnica Bucuresti <https://s.go.ro/p5ww9i7l>`__ - contine doar partea de algebra si analiza matematica. Daca vreti sa cumparati culegerea o puteti lua de pe  `site-ul oficial <https://culegeriadmitere.ro/culegeri/89-teste-matematica-2020-admitere-upb.html>`__

