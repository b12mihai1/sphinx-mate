---
title: Structuri de date dinamice - Liste simplu înlănțuite, stive, cozi
layout: post
order: 2
---

# Liste simplu înlănțuite

## Articole scrise

A Linked List is a linear data structure used for storing a collection of elements. 
Unlike arrays, linked lists use nodes to store elements which are not stored in contiguous memory locations.
In [this FreeCodeCamp article](https://www.freecodecamp.org/news/how-linked-lists-work/), you will learn 
what linked lists are, how they work, and how to build one.

## Clipuri video

Videoclipuri în limba română realizate de prof. Dan Pracsiu:

**Liste simplu înlănțuite**:

   * [Alocarea dinamică a memoriei. Liste simplu înlănțuite - partea I](https://www.youtube.com/watch?v=l9AP6iec5IQ)
   * [Liste simplu înlănțuite - lecția a II a](https://www.youtube.com/watch?v=re-jkzb1Y7w)
   * [Liste simplu înlănțuite - partea a III a](https://www.youtube.com/watch?v=-Iz9HX0t9ZQ)

**Coadă**:

   * [Structura de date de tip coada](https://www.youtube.com/watch?v=sgFTW9nMRKE)
   * [Structura liniara Coada - aplicații practice](https://www.youtube.com/watch?v=WOgbzA-Alyw)

**Opționale/extra**:

   * [Liste simplu înlănțuite circulare](https://www.youtube.com/watch?v=Z-4xL_GHHZw)
   * [Liste liniare dublu înlănțuite](https://www.youtube.com/watch?v=LCuvvLzmXMg)