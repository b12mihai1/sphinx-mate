---
title: Modele de rezolvari subiecte Bacalaureat si Admitere
layout: post
order: 2
---

  * [Rezolvarea subiectului de la bac informatică, sesiunea iunie-iulie 2020](https://www.youtube.com/watch?v=GRRCjG0Ak5w) - Subiectele se pot descarca de [aici](https://www.pbinfo.ro/resurse/9dc152/examene/2020/E_d_Informatica_2020_sp_MI_C_var_06_LRO.pdf)


   * [Subiectele aferente bacalaureat 2021 sunt aici](https://www.pbinfo.ro/resurse/9dc152/examene/2021/iunie-iulie/E_d_Informatica_2021_sp_MI_C_var_01_LRO.pdf)
       * [TeleŞcoala: Informatică – Rezolvare subiect Bacalaureat 2021 – prima parte](https://www.youtube.com/watch?v=kcpaWbqFy-I)
       * [TeleŞcoala: Informatică – Rezolvare subiect Bacalaureat iunie 2021 – partea a II-a](https://www.youtube.com/watch?v=_S7Eit4scmk)


   * [Bac iunie 2023 - rezolvarea subiectelor de la informatică](https://www.youtube.com/watch?v=h4c6sygc9k8) - Subiectele se pot descarca de [aici](https://www.pbinfo.ro/resurse/9dc152/examene/2023/E_d_Informatica_2023_sp_MI_C_var_05_LRO.pdf)


   * [TeleŞcoala: Informatică clasa a XII-a - Rezolvarea testului de antrenament 2](https://www.youtube.com/watch?v=smmbjVXctBs) - Subiectele aferente acestui videoclip [se afla aici](https://colaborare.rocnee.eu/files/testeantrenament/2020/T1_03.04.2020/teste/bac/Bac_2020_E_d_Informatica_teste/Bac_2020_E_d_Informatica_teste/E_d_Informatica_2020_sp_MI_C_var_test_02.pdf)


   * [TeleŞcoala: Informatică clasa a XII-a - Rezolvarea testului de antrenament 7](https://www.youtube.com/watch?v=ZHamh1t8v4c) - Subiectele aferente acestui videoclip [se afla aici](https://colaborare.rocnee.eu/files/testeantrenament/2020/T2_22.04.2020/teste/bac/Bac_2020_E_d_Informatica_teste/Bac_2020_E_d_Informatica_teste/Bac_2020_E_d_Informatica_sp_Mate-Info_teste/E_d_Informatica_2020_sp_MI_C_var_test_07.pdf)


   * [2021 - UPB îți oferă cursuri de pregătire gratuite pentru ADMITERE](https://www.youtube.com/live/kDJzBnKFyV0?feature=share&t=14725)

   * [2022 - Cursuri de pregătire pentru admiterea la UPB](https://www.youtube.com/live/nTY4FS9qMR4?feature=share&t=14841)