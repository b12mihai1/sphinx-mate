---
title: Manuale Informatica
layout: post
order: 1
---

Manuale de informatică (aprobate MECS)
--------------------------------------

 * [Tudor Sorin - Manual de informatică intensiv, clasa a IX-a](https://1drv.ms/b/s!Ao4rAdqiT2b_hopZjL1-ycmSOrtg2g?e=zgpREv)
 * [Vlad Huţanu, Tudor Sorin - INFORMATICĂ INTENSIV, clasa a XI-a](https://1drv.ms/b/s!Ao4rAdqiT2b_hpg7JR2kepq033HAwQ?e=rYkDr5)
 * [Editura Didactică și Pedagogică - Manual de informatică intensiv, clasa a X-a](https://1drv.ms/b/s!Ao4rAdqiT2b_hopXOFQTppyBh2Je8w?e=q95x8G)
 * [Vlad Tudor Huţanu, Carmen Popescu - Manual de informatică intensiv, clasa a XII-a](https://1drv.ms/b/s!Ao4rAdqiT2b_hpg6Wf_tMBzlvMx8KQ?e=jc3AND)

Programe școlare liceu
-----------------------

[Link programe clasele IX-XII](https://1drv.ms/u/s!Ao4rAdqiT2b_hopcSXUsQ2rj9CNEfg?e=uFs1at)