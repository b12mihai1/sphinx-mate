---
title: Visual Studio Code Setup
layout: post
order: 4
---

Setting up with Windows Subsystem for Linux
--------------------------------------------

Official guide from Microsoft: [Developing in WSL](https://code.visualstudio.com/docs/remote/wsl)

 * Accesati urmatorul link: [https://code.visualstudio.com/download](https://code.visualstudio.com/download)
 * Descarcati versiunea pe 64 biti din dreptul liniei System Installer
 * Deschideti fisierul descarcat, next, next, next
 * Puteti alege sa adaugati in context menu optiunea Open with Code. Asta va va permite sa dati click dreapta pe un fisier sau un folder si apoi sa-l deschideti instant in VSCode

In primul rand, avem 2 versiuni de WSL. Versiunea 1 este un emulator creat de Microsoft pentru kernel-ul de Linux. Versiunea 2 este chiar un kernel de Linux ce ruleaza intr-o masina virtuala. Puteți citi pe [documentația Microsoft](https://docs.microsoft.com/en-us/windows/wsl/compare-versions)
despre cele două versiuni de WSL. 

 1. Cautam PowerShell in search bar, si deschidem ca si administrator
 2. Activam feature-ul de WSL folosind urmatoarea comanda:

    ```
    dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
    ```

    Acum intervin versiunile despre care v-am vorbit. WSL 2, fiind o masina virtuala foarte light, necesita optiunea de virtualizare activa din BIOS. 

    Aveti aici un scurt ghid pentru activare: [https://bce.berkeley.edu/enabling-virtualization-in-your-pc-bios.html](https://bce.berkeley.edu/enabling-virtualization-in-your-pc-bios.html)

    Totusi, daca nu doriti sa va activati aceasta optiune ( care, totusi, va va fi necesara la alte materii in viitor ), puteti da restart calculatorului si sari la pasul 9
    De asemenea, daca versiunea voastra de windows nu este Windows 10, build 1903+, puteti da restart si sari la pasul 9

 3. Acum activam feature-ul de masina virtuala cu urmatoarea comanda:

    ```
    dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
    ```

 4. Restart PC
 5. Descarcam urmatorul fisier si il instalam: [wsl_update_x64.msi](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi)
 6. Deschidem din nou PowerShell ca si administrator
 7. Rulați comanda:

    ```
    wsl --set-default-version 2
    ```

 8. Deschidem Microsoft Store
 9. Cautam Ubuntu si alegem versiunea simpla, pe care o instalam
 10. Dupa instalare cautam Ubuntu in Start si deschidem
 11. Introducem un username si o parola pentru sistemul nostru de Linux
 12. Pentru lucru la informatica in terminalul deschis avem nevoie de rulat urmatoarea comanda pentru 
 a instala pachetele necesare compilarii programelor:

    ```
    sudo apt update && sudo apt install -y binutils gcc g++ make
    ```


Advanced users
--------------

Recommended is to have a virtual machine with Linux and use [SSH Remote Development](https://code.visualstudio.com/docs/remote/ssh)

Ready-made virtual machines can be downloaded from [osboxes](https://www.osboxes.org). Recommended software for starting a virtual machine, free and
easy to configure is [VirtualBox](https://www.virtualbox.org).