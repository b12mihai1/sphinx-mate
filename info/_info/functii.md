---
title: Functii
layout: post
order: 2
---

# Documentație suplimentară

[Subprograme. Noțiuni, exemple, modul de funcționare a unui program](https://1drv.ms/b/s!Ao4rAdqiT2b_hppuGdhs4URlLXw-YA?e=7g5V3k)

Resurse pbinfo:

*   [Subprograme](https://www.pbinfo.ro/articole/3656/subprograme)
    *   [Anatomia unei funcții C++](https://www.pbinfo.ro/articole/3662/anatomia-unei-functii-c-c)
    *   [Definirea și declararea unei funcții C++](https://www.pbinfo.ro/articole/3664/definirea-si-declararea-unei-functii-c-c)
    *   [Declararea variabilelor](https://www.pbinfo.ro/articole/3665/declararea-variabilelor)
    *   [Transmiterea parametrilor](https://www.pbinfo.ro/articole/3666/transmiterea-parametrilor)
    *   [Cum funcționează stiva?](https://www.pbinfo.ro/articole/3874/cum-functioneaza-stiva)
    *   [Mai mult despre funcții](https://www.pbinfo.ro/articole/3667/mai-mult)

# Introducere în funcții

În C/C++ noţiunea de funcţie este esenţială, deoarece asigură un mecanism de abstractizare a controlului: rezolvarea
unei părţi a problemei poate fi încredinţată unei funcţii, moment în care suntem preocupaţi de ce face funcţia,
fără a intra în detalii privind cum face funcţia anumite operaţii. Însăşi programul principal este o funcţie cu
numele **`main()`**, iar programul C/C++ este reprezentat de o mulţime de definiri de variabile şi de funcţii.

Funcţiile pot fi clasificate în:
 * funcţii care întorc un rezultat
 * funcţii care nu întorc nici un rezultat (similare procedurilor din Pascal).

Apelul (referirea sau utilizarea) unei funcţii se face prin:

**`nume_funcţie (listă_parametri_efectivi)`**

Acesta poate apare ca o instrucţiune:

**`nume_funcţie (listă_parametri_efectivi);`**

Pentru funcţiile care întorc un rezultat apelul de funcţie poate apare ca operand într-o expresie. De exemplu:

```cpp
y=sin(x);
nr_zile=bisect(an)+365;
```

Se remarcă faptul că lista de argumente (sau de parametri efectivi) poate fi vidă.
Funcţiile comunică între ele prin lista de argumente şi prin valorile întoarse de funcţii. Comunicarea poate fi
realizată şi prin variabilele externe, definite în afara tuturor funcţiilor.

# Definiții de funcții

În utilizarea curentă, o funcţie trebuie să fie definită înainte de a fi apelată. Aceasta impune o definire a funcţiilor
programului în ordinea sortării topologice a acestora: astfel mai întâi se vor defini funcţiile care nu apelează alte
funcţii, apoi funcţiile care apelează funcţii deja definite. Este posibil să eliminăm această restricţie, lucru pe care
îl vom trata ulterior.
O funcţie se defineşte prin antetul şi corpul funcţiei.
Funcţiile nu pot fi incluse unele în altele; toate funcţiile se declară pe acelaşi nivel cu funcţia main().
In versiunile mai vechi ale limbajului, în antetul funcţiei parametrii sunt numai enumeraţi, urmând a fi declaraţi
ulterior. Lista de parametri, în acest caz, este o enumerare de identificatori separaţi prin virgule.

```cpp
tip_rezultat nume_functie( lista_de_parametri_formali )
{ declarare_parametri_formali;
alte_declaratii;
instructiuni;
}
```

In mod uzual parametrii funcţiei se declară în antetul acesteia. Declararea parametrilor se face printr-o listă de
declaraţii de parametri, cu elementele separate prin virgule.

```cpp
tip_rezultat nume_functie( tip nume, tip nume,... )
{ declaratii;
instructiuni;
}
```

In cazul în care apelul funcţiei precede definiţia, trebuie dat, la începutul textului sursă, un prototip al funcţiei,
care să anunţe că definiţia funcţiei va urma şi să furnizeze tipul rezultatului returnat de funcţie şi tipul
parametrilor, pentru a permite compilatorului să facă verificările necesare.
Prototipul unei funcţii are un format asemănător antetului funcţiei şi serveşte pentru a informa compilatorul
asupra:
  * tipului valorii furnizate de funcţie;
  * existenţa şi tipurile parametrilor funcţiei

Spre deosebire de un antet de funcţie, un prototip se termină prin **`tip nume( lista tipurilor parametrilor formali);`**

Din prototip interesează numai tipurile parametrilor, nu şi numele acestora, motiv pentru care aceste nume pot fi
omise.

```cpp
void f(); /*functie fara parametri care nu intoarce nici un rezultat*/
int g(int x, long y[], double z);
int g(int, long[], double); /*aici s-au omis numele parametrilor*/
```

Dintre toate funcţiile prezente într-un program C prima funcţie lansată în execuţie este main(), independent de
poziţia pe care o ocupă în program.
Apelul unei funcţii **`g()`**, lansat din altă funcţie **`f()`** reprezintă un transfer al controlului din funcţia **`f()`**, din
punctul în care a fost lansat apelul, în funcţia **`g()`**. După terminarea funcţiei **`g()`** sau la întâlnirea instrucţiunii
return se revine în funcţia **`f()`** în punctul care urmează apelului **`g()`**. Pentru continuarea calculelor în **`f()`**,
la revenirea din **`g()`** este necesară salvarea stării variabilelor (contextului) din **`f()`** în momentul transferului
controlului. La revenire în **`f()`**, contextul memorat a lui **`f()`** va fi refăcut.

O funcţie apelată poate, la rândul ei, să apeleze altă funcţie; nu există nici o limitare privind numărul de apeluri
înlănţuite.

## Exemplu 1

O fracţie este cunoscută prin numărătorul **`x`** şi numitorul **`y`**, valori întregi fără semn. Să se
simplifice această fracţie. Simplificarea se va face prin cel mai mare divizor comun al numerelor **`x`** şi **`y`**. 
Vom utiliza o funcţie având ca parametri cele două numere, care întoarce ca rezultat, cel mai mare divizor comun a lor.
Funcţia **`main()`** apelează funcţia **`cmmdc()`** transmiţându-i ca argumente pe **`x`** şi **`y`**.
Funcţia **`cmmdc()`** întoarce ca rezultat funcţiei **`main()`**, valoarea celui mai mare divizor comun.

```cpp
#include <iostream>

using namespace std;

int cmmdc(int u, int v)
{
    int r;

    do {
        r = u % v;
        u = v;
        v = r;
    } while (r);

    return u;
}

int main()
{ 
    int x, y, z;

    cin >> x >> y;

    z = cmmdc(x,y);
    x = x / z;
    y = y / z;

    cout << "x/y = " << x << y << endl;
    return 0;
}
```

# Comunicarea între funcţii prin variabile externe. Efecte laterale ale funcţiilor.

Comunicarea între funcţii se poate face prin variabile externe tuturor funcţiilor; acestea îşi pot prelua date şi pot
depune rezultate în variabile externe. În exemplul cu simplificarea fracţiei vom folosi variabilele externe **`a, b, c`**.
Funcţia **`cmmdc()`** calculează divizorul comun maxim dintre **`a`** şi **`b`** şi depune rezultatul în **`c`**. 
Întrucât nu transmite date prin lista de parametri şi nu întoarce vreun rezultat, funcţia va avea prototipul **`void cmmdc()`**:

```cpp
#include <iostream>

using namespace std;

int a, b, c; /* variabile externe */

void cmmdc()
{
    int r;

    do {
        r = a % b;
        a = b;
        b = r;
    } while (r);
    
    c = a;   
}

int main()
{ 
    cin >> a >> b;

    cmmdc();
    a = a / c;
    b = b / c;

    cout << "a/b = " << a << b << endl;

    return 0;
}
```

Dacă se execută acest program, se constată un rezultat ciudat, şi anume, orice fracţie, prin simplificare ar fi adusă
la forma **`1/0`**! Explicaţia constă în faptul că funcţia **`cmmdc()`** prezintă efecte laterale, şi anume modifică
valorile variabilelor externe **`a, b, c`**; la ieşirea din funcţie **`a==c şi b==0`**, ceea ce explică rezultatul.

Aşadar, un efect lateral (sau secundar),reprezintă modificarea de către funcţie a unor variabile externe.
În multe situaţii aceste efecte sunt nedorite, duc la apariţia unor erori greu de localizat, făcând programele
neclare, greu de urmărit, cu rezultate dependente de ordinea în care se aplică funcţiile care prezintă efecte
secundare. Astfel într-o expresie în care termenii sunt apeluri de funcţii, comutarea a doi termeni ar putea
conduce la rezultate diferite!

Vom corecta rezultatul, limitând efectele laterale prin interzicerea modificării variabilelor externe a şi b, ceea ce
presupune modificarea unor copii ale lor în funcţia **`cmmdc()`** sau din programul de apelare.

```cpp
#include <iostream>

int a, b, c; /* variabile externe */

void cmmdc()
{
    int r, ca, cb;
    ca = a;
    cb = b;

    do {
        r = ca % cb;
        ca = cb;
        cb = r;
    } while (r);
    
    c = ca;
}
```

Singurul efect lateral permis în acest caz – modificarea lui c asigură transmiterea rezultatului către funcţia
apelantă.

Soluţia mai naturală şi mai puţin expusă erorilor se obţine realizând comunicaţia între funcţia **`cmmdc()`** şi
funcţia **`main()`** nu prin variabile externe, ci prin parametri, folosind o funcţie care întoarce ca rezultat cmmdc.

Transmiterea parametrilor prin valoare, mecanism specific limbajului C/C++, asigură păstrarea intactă a parametrilor
actuali **`x`** şi **`y`**, deşi parametrii formali corespunzători: **`u`** şi **`v`** se modifică!
Parametrii actuali **`x`** şi **`y`** sunt copiaţi în variabilele **`u`** şi **`v`**, astfel încât se modifică copiile lor nu şi **`x`** şi **`y`**.

In fişierul sursă funcţiile pot fi definite în orice ordine. Mai mult, programul se poate întinde în mai multe fişiere
sursă. Definirea unei funcţii nu poate fi totuşi partajată în mai multe fişiere.

O funcţie poate fi apelată într-un punct al fişierului sursă, dacă în prealabil a fost definită în acelaşi fişier sursă,
sau a fost anunţată.

## Exemplu 2

```cpp
#include <iostream>

using namespace std;

long fact(int); // prototipul anunta functia

int main()
{ 
    cout << fact(5) << endl; // apel functie
    cout << fact(10) << endl; // apel functie
    return 0;
}

long fact(int n) // antet functie
{ 
    long f = 1; // corp functie
    int i;
    for (i = 2; i <= n; i++)
        f = f * i;

    return f;
}
```

# Funcţii care apelează alte funcţii.

Programul principal (funcţia  **`main()`** ) apelează alte funcţii, care la rândul lor pot apela alte funcţii. Ordinea
definiţiilor funcţiilor poate fi arbitrară, dacă se declară la începutul programului prototipurile funcţiilor. In caz
contrar definiţiile se dau într-o ordine în care nu sunt precedate de apeluri ale funcţiilor

## Exemplu 3

Pentru o valoare întreagă şi pozitivă n dată, să se genereze [triunghiul lui Pascal](https://www.pbinfo.ro/articole/10864/triunghiul-lui-pascal),
adică combinările:

$$C_0^0$$


$$C_1^0   C_1^1$$


$$\ldots$$


$$C_n^0  C_n^1  \ldots  C_n^n$$

Combinările vor fi calculate utilizând formula cu factoriale:  $$C_n^p = \frac{n!}{p! \cdot (n-p)!}$$

```cpp
#include <iostream>

using namespace std;

long fact(int); /*prototip functie factorial*/
long comb(int,int); /*prototip functie combinari*/

int main() /*antet functie main*/
{ 
    int k, j, n;

    cin >> n;
    for (k = 0; k <= n; k++) { 
        for (j = 0; j <= k; j++)
            cout << comb(k, j) << " ";
        cout << endl;
    }

    return 0;
}

long fact(int n) /* antet functie fact */
{ 
    long f = 1; /* corp functie fact */
    int i;
    for (i = 2; i <= n; i++)
        f = f * i;

    return f;
}

long comb(int n, int p) /* antet functie comb */
{
    return (fact(n) / fact(p) / fact(n-p));
}
```