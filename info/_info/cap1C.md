---
title: C++ Basics
layout: post
order: 2
---

# Structura unui program în C 

Linkuri recomandate:
 * [https://www.cplusplus.com/doc/tutorial/program_structure/](https://www.cplusplus.com/doc/tutorial/program_structure/)
 * [https://www.pbinfo.ro/articole/59/introducere-in-cpp](https://www.pbinfo.ro/articole/59/introducere-in-cpp)

Utilizare CodeBlocks: [cplusplus.com](https://www.cplusplus.com/doc/tutorial/introduction/codeblocks/),
[pbinfo](https://www.pbinfo.ro/?pagina=intrebari-afisare&id=27)

# Tipuri de date si variabile

 * [Tipuri de date C++](https://www.pbinfo.ro/articole/58/tipuri-de-date-c-cpp)
 * [Tipul char](https://www.pbinfo.ro/articole/26016/tipul-char)
 * [Conversii de tip](https://www.pbinfo.ro/articole/26007/conversii-de-tip-in-cpp)
 * [Variabile și constante C++](https://www.pbinfo.ro/articole/57/variabile-si-constante-c-cpp)
 * [cplusplus.com: Variables and types](https://www.cplusplus.com/doc/tutorial/variables/)
 * [cplusplus.com: Constants](https://www.cplusplus.com/doc/tutorial/constants/)

# I/O (Intrări/ieșiri)

 * [cplusplus.com: Basic Input/Output](https://www.cplusplus.com/doc/tutorial/basic_io/)
 * [Intrări/ieșiri în C++](https://www.pbinfo.ro/articole/60/intrari-iesiri-in-cpp)
 * [Citiri și scrieri cu format](https://www.pbinfo.ro/articole/16018/citiri-si-scrieri-cu-format-in-cpp)

# Operatori

 * [cplusplus.com: Operators](https://www.cplusplus.com/doc/tutorial/operators/)
 * [Operatori C++](https://www.pbinfo.ro/articole/61/operatori-cpp)
 * [Operații logice](https://www.pbinfo.ro/articole/21315/operatii-logice)
 * [Operatorii de incrementare/decrementare](https://www.pbinfo.ro/articole/26005/operatorii-de-incrementare-decrementare)
 * [Operatorul conditional ?](https://www.pbinfo.ro/articole/26006/operatorul-conditional)
 * [Funcții predefinite: sqrt, pow, trigonometrice etc](https://www.pbinfo.ro/articole/8598/functii-cpp-predefinite)

**RECOMANDARE: parcurgeți acest capitol** - [Captilul 3 - Elemente de bază ale limbajului C++](https://1drv.ms/b/s!Ao4rAdqiT2b_hplNZ-KWDfTmBlwy_g?e=Qw71XP) 
este din manulul lui Tudor Sorin. Conține o sinteză a tuturor aspectelor prezentate mai sus, cu exemple/probleme rezolvate.

# Structuri de control

**RECOMANDARE: parcurgeți acest capitol**:

[Capitolul 4 - Instrucțiunile limbajului C++](https://1drv.ms/b/s!Ao4rAdqiT2b_hplM32z15t83GuRzGg?e=j0n9jX) - este din manulul lui Tudor Sorin. 
Are și exemple de programe/probleme rezolvate. Recomand să scrieți de mână exemplele în Codeblocks
și să le rulați. 


**Articole bune pbinfo, cu exemple**:

 * [Structura liniară](https://www.pbinfo.ro/articole/68/structura-liniara)
 * [Structuri alternative](https://www.pbinfo.ro/articole/70/structuri-alternative)
 * [Structuri repetitive](https://www.pbinfo.ro/articole/71/structuri-repetitive)
    * [while](https://www.pbinfo.ro/articole/26009/instructiunea-while)
    * [do…while](https://www.pbinfo.ro/articole/26010/instructiunea-do-while)
    * [for](https://www.pbinfo.ro/articole/26011/instructiunea-for)
    * [break](https://www.pbinfo.ro/articole/26012/instructiunea-break)
    * [continue](https://www.pbinfo.ro/articole/26013/instructiunea-continue)

**Pagina de referință C++**:

 * [cplusplus.com: Statements and flow control](https://www.cplusplus.com/doc/tutorial/control/)