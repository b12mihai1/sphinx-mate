---
layout: home
---

<ul>
  {% for mypage in site.info %}
    <li>
      <a href="{{ mypage.url }}">{{ mypage.title }}</a>
    </li>
  {% endfor %}
</ul>
